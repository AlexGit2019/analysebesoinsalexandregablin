public class NombrePositif {
    private int nombre;
    public NombrePositif increment() {
        return new NombrePositif(++this.nombre);
    }
    public NombrePositif(int nombre) {
        if (nombre < 0) {
            throw new IllegalArgumentException();
        }
        this.nombre = nombre;
    }

    public void setNombre(int valeur) {
        if (valeur >= 0) {
            this.nombre = valeur;
        }
    }

    public int getNombre() {
        return this.nombre;
    }

    @Override
    public boolean equals(Object obj) {
        try {
            return this.nombre == ((NombrePositif)obj).getNombre();
        }
        catch (ClassCastException e) {
            return false;
        }
    }

    public NombrePositif ajout(int i) {
        return new NombrePositif(this.nombre + i);
    }

    public boolean estSuperieurA(NombrePositif nombrePositif) {
        return this.nombre > nombrePositif.nombre;
    }

    public NombrePositif retrait(int i) {
        return new NombrePositif(this.nombre - i);
    }
    public NombrePositif retrait(NombrePositif i) {
        return new NombrePositif(this.nombre - i.nombre);
    }
    public NombrePositif decrement() {
        return new NombrePositif(this.nombre - 1);
    }
}
