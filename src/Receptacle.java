import java.util.Objects;
import java.util.Optional;

public class Receptacle {
    private Optional optionalGobelet;

    public Receptacle(Optional optionalGobelet) {
        this.optionalGobelet = Objects.requireNonNull(optionalGobelet);
    }
}
