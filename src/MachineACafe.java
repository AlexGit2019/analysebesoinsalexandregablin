import java.util.Objects;
import java.util.Optional;

public class MachineACafe {
    private NombrePositif argentEncaisse;
    private NombrePositif stockEau;
    private NombrePositif dosesDeSucreCafeCourant;
    private NombrePositif stockTouillettes;
    private NombrePositif stockGobelets;
    private NombrePositif stockCafe;
    private NombrePositif stockSucre;
    private NombrePositif nombreCafesServis;
    private static final NombrePositif limiteStockCafe = new NombrePositif(30);
    private static final NombrePositif limiteStockSucre = new NombrePositif(100);
    private static final NombrePositif limiteStockTouillettes = new NombrePositif(200);
    private static final NombrePositif limiteNombreGobelets = new NombrePositif(50);

    public static NombrePositif getLimiteStockCafe() {
        return MachineACafe.limiteStockCafe;
    }

    public static NombrePositif getLimiteStockSucre() {
        return MachineACafe.limiteStockSucre;
    }

    public static NombrePositif getLimiteStockTouillettes() {
        return MachineACafe.limiteStockTouillettes;
    }

    public MachineACafe(NombrePositif stockGobelets, NombrePositif stockCafe, NombrePositif nbCafesServis, NombrePositif argentEncaisse, NombrePositif stockEau, NombrePositif stockSucre, NombrePositif stockTouillettes) {
            this.stockGobelets = Objects.requireNonNull(stockGobelets);
            this.stockCafe = Objects.requireNonNull(stockCafe);
            this.nombreCafesServis = Objects.requireNonNull(nbCafesServis);
            this.argentEncaisse = Objects.requireNonNull(argentEncaisse);
            this.stockEau = Objects.requireNonNull(stockEau);
            this.stockSucre = Objects.requireNonNull(stockSucre);
            this.dosesDeSucreCafeCourant = new NombrePositif(0);
            this.stockTouillettes = Objects.requireNonNull(stockTouillettes);
            if (stockCafe.getNombre() > 30 || stockSucre.getNombre() > 100 || stockTouillettes.getNombre() > 200) {
                throw new IllegalArgumentException();
            }

    }


    public static NombrePositif getLimiteNombreGobelets() {
        return MachineACafe.limiteNombreGobelets;
    }


    public void insererMonnaie(int nombreCentimesInseres, boolean tasse) {
        if (nombreCentimesInseres < 40) {
            return;
        }
        boolean recipientDisponible = this.stockGobelets.getNombre() != 0 || tasse;
        boolean consommablesDisponibles = this.stockEau.getNombre() != 0 && this.stockCafe.getNombre() != 0 && this.stockSucre.getNombre() >= this.dosesDeSucreCafeCourant.getNombre();
        if (recipientDisponible && consommablesDisponibles) {
            this.argentEncaisse = this.argentEncaisse.ajout(nombreCentimesInseres);
            this.nombreCafesServis = this.nombreCafesServis.ajout(1);
            if (!tasse) {
                this.stockGobelets = this.stockGobelets.retrait(1);
            }
            this.stockSucre = this.stockSucre.retrait(this.dosesDeSucreCafeCourant);
            this.stockCafe = this.stockCafe.retrait(1);
            this.dosesDeSucreCafeCourant.setNombre(0);
            if (this.stockTouillettes.getNombre() > 0) {
                this.stockTouillettes = this.stockTouillettes.retrait(1);
            }
        }
    }

    public NombrePositif getNombreCafesServis() {
        return new NombrePositif(this.nombreCafesServis.getNombre());
    }

    public void ajouterGobelets(int nbGobelets) {
        int stockGobeletApresAjout = this.stockGobelets.getNombre() + nbGobelets;
        int nbMaxGobelets = 50;
        if (stockGobeletApresAjout <= nbMaxGobelets) {
            this.stockGobelets = this.stockGobelets.ajout(nbGobelets);
        }
        //Si le stock de gobelets après ajout dépasse 50 alors on met le nombre de gobelets à 50
        else {
            this.stockGobelets.setNombre(nbMaxGobelets);
        }
    }

    public void ajouterCafe(int nbDoses) {
        if (this.stockCafe.getNombre() + nbDoses < 30) {
            this.stockCafe = this.stockCafe.ajout(nbDoses);
        } else {
            this.stockCafe.setNombre(30);
        }
    }


    public void annuler () {
        this.argentEncaisse = this.argentEncaisse.retrait(40);
        this.nombreCafesServis = this.nombreCafesServis.retrait(1);
    }

    public NombrePositif getNombreGobelets() {
        return new NombrePositif(this.stockGobelets.getNombre());
    }

    public NombrePositif getStockCafe() {
        return new NombrePositif(this.stockCafe.getNombre());
    }

    public NombrePositif getArgentEncaisse() {
        return new NombrePositif(this.argentEncaisse.getNombre());
    }

    public NombrePositif getStockSucre() {
        return new NombrePositif(this.stockSucre.getNombre());
    }

    public void augmenterDoseSucre() {
        if (this.dosesDeSucreCafeCourant.ajout(1).getNombre() <= 5) {
            this.dosesDeSucreCafeCourant = this.dosesDeSucreCafeCourant.ajout(1);
        }
    }

    public void ajouterDosesSucre(int i) {
        NombrePositif nbLimitesDoses = new NombrePositif(100);
        NombrePositif quantiteSucreApresAjout = this.stockSucre.ajout(i);
        if (quantiteSucreApresAjout.estSuperieurA(nbLimitesDoses)) {
            this.stockSucre.setNombre(nbLimitesDoses.getNombre());
        }
        else {
            this.stockSucre = this.stockSucre.ajout(i);
        }
    }

    public NombrePositif getStockTouillettes() {
        return new NombrePositif(this.stockTouillettes.getNombre());
    }

    public void ajouterTouillettes(int nbTouillettes) {
        NombrePositif nbTouilettesTheoriqueApresAjout = this.stockTouillettes.ajout(nbTouillettes);
        this.stockTouillettes.setNombre(Math.min(nbTouilettesTheoriqueApresAjout.getNombre(), 200));
    }

    public NombrePositif getDosesSucreCafeCourant() {
        return this.dosesDeSucreCafeCourant;
    }

    public void diminuerDoseSucreCafeCourant() {
        this.dosesDeSucreCafeCourant = this.dosesDeSucreCafeCourant.decrement();
    }
}
