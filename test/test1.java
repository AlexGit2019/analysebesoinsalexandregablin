import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class test1 {
    @Test
    @DisplayName("QUAND j'insère 40 cts, ALORS le café coule ET une dose de café est consommée")
    public void testCafeCoule() {
        MachineACafe machineACafe = new BuilderMachineACafe().stockCafe(new NombrePositif(30)).stockEau(new NombrePositif(10)).build();
        NombrePositif nbDosesStockeesInitial = machineACafe.getStockCafe();
        NombrePositif nbDosesStockeesAttendu = nbDosesStockeesInitial.decrement();
        NombrePositif nbCafesServisInitial = machineACafe.getNombreCafesServis();
        NombrePositif nbCafesServisAttendu = nbCafesServisInitial.increment();
        machineACafe.insererMonnaie(40, true);
        assertEquals(machineACafe.getNombreCafesServis(), nbCafesServisAttendu);
        assertEquals(nbDosesStockeesAttendu, machineACafe.getStockCafe());
    }
    @ParameterizedTest
    @DisplayName("ETANT DONNÉ que le stock de gobelets est de 50 - <x> QUAND on rajoute <x+1> gobelets ALORS le stock de gobelets est de 50.")
    @ValueSource(ints = {1,0})
    public void testMaxStockGobelets(int x) {
        NombrePositif nombreLimiteDeGobelets = MachineACafe.getLimiteNombreGobelets();
        MachineACafe machineACafe = new BuilderMachineACafe().stockGobelets(nombreLimiteDeGobelets.retrait(x)).build();
        machineACafe.ajouterGobelets(x + 1);
        assertEquals(nombreLimiteDeGobelets,machineACafe.getNombreGobelets());
    }
    @ParameterizedTest
    @ValueSource(ints = {0, 1, 200})
    @DisplayName("ÉTANT donné qu'il reste 200 - <x> touilettes, QUAND on rajoute <x> + 1 touillettes, ALORS le nombre total de touillettes est de 200")
    public void nbTouilettesRemis200SiQuotaTouillettesDepasse(int x) {
        NombrePositif limiteTouillettes = MachineACafe.getLimiteStockTouillettes();
        MachineACafe machineACafe = new BuilderMachineACafe().stockTouillettes(limiteTouillettes.retrait(x)).build();
        machineACafe.ajouterTouillettes(x+1);
        assertEquals(limiteTouillettes, machineACafe.getStockTouillettes());
    }

    @ParameterizedTest
    @ValueSource(ints = {0,1,2,10,200})
    @DisplayName("QUAND on rajoute <x> touillettes au stock initial Sn de touillettes ET que Sn + <x> <= 200 , ALORS le stock de touillettes est augmenté de <x>")
    public void ajoutXTouillettesAugmenteStockDeX(int x) {
        MachineACafe machineACafe = new BuilderMachineACafe().stockTouillettes(new NombrePositif(0)).build();
        NombrePositif stockTouillettesInitial = machineACafe.getStockTouillettes();
        NombrePositif stockTouillettesAttendu = stockTouillettesInitial.ajout(x);
        machineACafe.ajouterTouillettes(x);
        assertEquals(stockTouillettesAttendu, machineACafe.getStockTouillettes());
    }
    @ParameterizedTest
    @DisplayName("ETANT DONNÉ que le stock de café est de 30 - <x> doses QUAND on ajoute <x> + 1 dose ALORS le stock de café est de 30 doses CAS 0, 1, 30")
    @ValueSource(ints = {0,1,30})
    public void testMaxStockCafe(int x) {
        NombrePositif limiteDoses = MachineACafe.getLimiteStockCafe();
        MachineACafe machineACafe = new BuilderMachineACafe().stockCafe(limiteDoses.retrait(x)).build();
        machineACafe.ajouterCafe(x + 1);
        assertEquals(limiteDoses,machineACafe.getStockCafe());
    }
    @Test
    @DisplayName("QUAND on insère plus de 40 centimes, ALORS un café est servi ET on encaisse tout l'argent")
    public void testSuperieurMonnaie(){
        MachineACafe machine = new BuilderMachineACafe().stockCafe(new NombrePositif(30)).stockSucre(new NombrePositif(10)).stockEau(new NombrePositif(10)).build();
        NombrePositif argentInitalementEncaisse = machine.getArgentEncaisse();
        NombrePositif argentEncaisseAttendu = argentInitalementEncaisse.ajout(44);
        NombrePositif nombreCafesServisInitial = machine.getNombreCafesServis();
        NombrePositif nombreCafesServisAttendu = nombreCafesServisInitial.increment();
        machine.insererMonnaie(44, true);
        assertEquals(nombreCafesServisAttendu, machine.getNombreCafesServis());
        assertEquals(argentEncaisseAttendu, machine.getArgentEncaisse());
    }
    @Test
    @DisplayName("ÉTANT DONNÉ qu'il n'y à plus de café,QUAND ON INSERE 40 centimes, ALORS le solde final est le même que le solde initial ET aucun café n'a coulé")
    public void testArgentRenduSiPlusCafe(){
        MachineACafe machine = new BuilderMachineACafe().stockCafe(new NombrePositif(0)).build();
        NombrePositif soldeInitial = machine.getArgentEncaisse();
        NombrePositif nbCafesServisInitial = machine.getNombreCafesServis();
        machine.insererMonnaie(40, true);
        assertEquals(soldeInitial,machine.getArgentEncaisse());
        assertEquals(machine.getNombreCafesServis(), nbCafesServisInitial);
    }
    @Test
    @DisplayName("ÉTANT DONNÉ qu'il n'y a plus de gobelets,QUAND on insère de la monnaie ET qu'il n'y a pas de tasse,ALORS on rend l'argent ET aucun café n'a coulé")
    public void testPlusDeGobelet(){
        MachineACafe machine = new BuilderMachineACafe().stockGobelets(new NombrePositif(0)).build();
        NombrePositif stockInitial = machine.getArgentEncaisse();
        NombrePositif nbCafesServisInitial = machine.getNombreCafesServis();
        machine.insererMonnaie(40, false);
        assertEquals(stockInitial,machine.getArgentEncaisse());
        assertEquals(machine.getNombreCafesServis(), nbCafesServisInitial);
    }
    @Test
    @DisplayName("ÉTANT donné qu'il n'y a plus d'eau, QUAND on insère de la monnaie, ALORS on rend l'argent ET aucun café n'est servi")
    public void testPlusDeau(){
        MachineACafe machine = new BuilderMachineACafe().stockEau(new NombrePositif(0)).build();
        NombrePositif stockInitial = machine.getArgentEncaisse();
        NombrePositif nbCafesInitialementServis = machine.getNombreCafesServis();
        machine.insererMonnaie(40,false);
        assertEquals(stockInitial, machine.getArgentEncaisse());
        assertEquals(machine.getNombreCafesServis(), nbCafesInitialementServis);

    }
    @Test
    @DisplayName("ÉTANT DONNÉ que l'utilisateur a commandé un café sucré, QUAND j'insère 40 cts ALORS un café coule")
    public void nombreCafesServisAugmenteSiCafeSucre(){
        MachineACafe machine = new BuilderMachineACafe().stockSucre(new NombrePositif(10)).build();
        NombrePositif nbDosesStockeesInitial = machine.getStockCafe();
        NombrePositif nbDosesStockeesAttendu = nbDosesStockeesInitial.decrement();
        NombrePositif nbCafesServisInitial = machine.getNombreCafesServis();
        NombrePositif nbCafesServisAttendu = nbCafesServisInitial.increment();
        machine.augmenterDoseSucre();
        machine.insererMonnaie(40,false);
        assertEquals(nbCafesServisAttendu,machine.getNombreCafesServis());
        assertEquals(nbDosesStockeesAttendu, machine.getStockCafe());

    }

    @Test
    @DisplayName("ETANT DONNÉ une machine à café avec suffisamment d'eau, de café ET de sucre, QUAND j'insère de la monnaie, ALORS le solde est mis à jour")
    public void testInsererMonnaieMetAJourSolde() {
        MachineACafe machineACafe = new BuilderMachineACafe().stockEau(new NombrePositif(10)).stockCafe(new NombrePositif(10)).stockSucre(new NombrePositif(10)).build();
        NombrePositif argentEncaisseInitial = machineACafe.getArgentEncaisse();
        int nbCentimesInseres = 40;
        NombrePositif argentEncaisseAttendu = argentEncaisseInitial.ajout(nbCentimesInseres);
        machineACafe.insererMonnaie(nbCentimesInseres,true);
        assertEquals(argentEncaisseAttendu, machineACafe.getArgentEncaisse());
    }
    @Test
    @DisplayName("ETANT DONNÉ que le café ne peut pas être préparé ET qu'il n'y a pas de tasse, QUAND on insère 40 cts ALORS un gobelet n'est pas consommé")
    public void testGobeletNonDepense() {
        MachineACafe machineACafe = new BuilderMachineACafe().stockEau(new NombrePositif(0)).build();
        NombrePositif stockGobeletInitial = machineACafe.getNombreGobelets();
        machineACafe.insererMonnaie(40,false);
        assertEquals(stockGobeletInitial,machineACafe.getNombreGobelets());
    }
    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5})
    @DisplayName("ÉTANT donné que l'utilisateur a choisi un café sucré contenant <x> doses de sucre, QUAND j'insère 40cts, ALORS <x> doses de sucre sont consommées")
    public void xDosesDeSucreConsommees(int x) {
        MachineACafe machineACafe = new BuilderMachineACafe().stockSucre(new NombrePositif(5)).build();
        NombrePositif stockSucreInitial = machineACafe.getStockSucre();
        NombrePositif stockSucreAttendu = stockSucreInitial.retrait(x);
        for (int i = 0; i < x; i++) {
            machineACafe.augmenterDoseSucre();
        }
        machineACafe.insererMonnaie(40,false);
        assertEquals(stockSucreAttendu, machineACafe.getStockSucre());
    }
    @ParameterizedTest
    @ValueSource(ints = {100,0,99})
    @DisplayName("ÉTANT donné qu'il reste 100 - <x> doses de sucres, QUAND le technicien rajoute <x> + 1 doses de sucre, ALORS le nombre de doses de sucre est de 100")
    public void nbDosesCafeResteA100(int x) {
        NombrePositif limiteStockSucre = MachineACafe.getLimiteStockSucre();
        MachineACafe machineACafe = new BuilderMachineACafe().stockSucre(limiteStockSucre.retrait(x)).build();
        machineACafe.ajouterDosesSucre(x+1);
        assertEquals(limiteStockSucre,machineACafe.getStockSucre());
    }

    @ParameterizedTest
    @ValueSource(ints = {0,1})
    @DisplayName("getStockTouillettes() retourne bien le nombre de touillettes dans le stock")
    public void getTouilettesRetourneTouilettesDansStock(int nbTouillettes) {
        MachineACafe machineACafe = new BuilderMachineACafe().stockTouillettes(new NombrePositif(nbTouillettes)).build();
        assertEquals(new NombrePositif(nbTouillettes), machineACafe.getStockTouillettes());
    }
    @ParameterizedTest
    @ValueSource(ints = {0,1,100})
    @DisplayName("QUAND le technicien remet <x> doses de sucre dans le stock, ALORS le stock est augmenté de <x>")
    public void ajoutDeXDosesDeSucreAugmenteLeStockDeX(int x) {
        MachineACafe machineACafe = new BuilderMachineACafe().stockSucre(new NombrePositif(0)).build();
        NombrePositif stockSucreInitial = machineACafe.getStockSucre();
        NombrePositif stockSucreAttendu = stockSucreInitial.ajout(x);
        machineACafe.ajouterDosesSucre(x);
        assertEquals(stockSucreAttendu, machineACafe.getStockSucre());
    }
    @Test
    @DisplayName("ÉTANT DONNÉ que l'utilisateur a choisi un café sucré, QUAND il n'y a plus de touillettes, ALORS un café coule")
    public void cafeSucreCouleSansTouillettes(){
        MachineACafe machineACafe = new BuilderMachineACafe().stockTouillettes(new NombrePositif(0)).build();
        NombrePositif nbCafesServis = machineACafe.getNombreCafesServis();
        NombrePositif nbCafesServisAttendu = nbCafesServis.increment();
        NombrePositif nbDosesStockeesInitial = machineACafe.getStockCafe();
        NombrePositif nbDosesStockeesAttendu = nbDosesStockeesInitial.decrement();
        machineACafe.augmenterDoseSucre();
        machineACafe.insererMonnaie(40,false);
        assertEquals(nbCafesServisAttendu,machineACafe.getNombreCafesServis());
        assertEquals(nbDosesStockeesAttendu, machineACafe.getStockCafe());
    }
    @Test
    @DisplayName("ÉTANT DONNÉ qu'un café sucré a fini d'être préparé, QUAND on insère 40cts, ALORS un café non sucré coule")
    public void doseDeSucreDuCafeCourantReinitialisee() {
        MachineACafe machineACafe = new BuilderMachineACafe().build();
        machineACafe.augmenterDoseSucre();
        machineACafe.insererMonnaie(40,true);
        assertEquals(new NombrePositif(0),machineACafe.getDosesSucreCafeCourant());
    }
    @Test
    @DisplayName("ETANT Donné que la personne a demandé <x> doses de sucre, QUAND il reste moins de <x> doses de sucre ALORS on rend la monnaie")
    public void siStockDeSucreInsuffisantAlorsMonnaieRendue() {
        MachineACafe machineACafe = new BuilderMachineACafe().stockSucre(new NombrePositif(0)).stockCafe(new NombrePositif(10)).build();
        NombrePositif argentInitialementEncaisse = machineACafe.getArgentEncaisse();
        for (int i = 0; i < 2; i++) {
            machineACafe.augmenterDoseSucre();
        }
        machineACafe.insererMonnaie(40,false);
        assertEquals(argentInitialementEncaisse, machineACafe.getArgentEncaisse());

    }
    @Test
    @DisplayName("QUAND on met 40cts ET qu'une tasse n'est pas détectée ALORS on décrémente le nombre de gobelets")
    public void nombreDeGobeletsDiminueQUAND40ctsInseres() {
        MachineACafe machineACafe = new BuilderMachineACafe().build();
        NombrePositif nombreGobeletsInitial = machineACafe.getNombreGobelets();
        NombrePositif nombreGobeletsAttendu = nombreGobeletsInitial.decrement();
        machineACafe.insererMonnaie(40, false);
        assertEquals(nombreGobeletsAttendu, machineACafe.getNombreGobelets());
    }
    @ParameterizedTest
    @ValueSource(ints = {0,50})
    @DisplayName("QUAND le technicien remet <x> gobelets ALORS on ajoute <x> au stock de gobelets")
    public void QUANDOnAjouteXGobeletsLeStockAugmenteDeX(int x) {
        MachineACafe machineACafe = new BuilderMachineACafe().stockGobelets(new NombrePositif(0)).build();
        NombrePositif stockGobeletsInitial = machineACafe.getNombreGobelets();
        NombrePositif nombreGobeletsAttendu = stockGobeletsInitial.ajout(x);
        machineACafe.ajouterGobelets(x);
        assertEquals(nombreGobeletsAttendu, machineACafe.getNombreGobelets());
    }
    @Test
    @DisplayName("ÉTANT donné que l'utilisateur a choisi un café sucré, QUAND il y a des touillettes ALORS une touillette est consommée")
    public void uneTouiletteEstConsommeeQUANDLutilisateurPrendUnCafeSucre() {
        MachineACafe machineACafe = new BuilderMachineACafe().stockTouillettes(new NombrePositif(10)).build();
        NombrePositif stockTouillettesInitial = machineACafe.getStockTouillettes();
        NombrePositif stockTouillettesAttendu = stockTouillettesInitial.decrement();
        machineACafe.augmenterDoseSucre();
        machineACafe.insererMonnaie(40, false);
        assertEquals(stockTouillettesAttendu, machineACafe.getStockTouillettes());
    }
    @ParameterizedTest
    @ValueSource(ints = {0,1,30})
    @DisplayName("QUAND le technicien remet <x> doses de café ALORS <x> doses sont ajoutées au stock CAS 0 1 30")
    public void remettreXDosesDeCafeAjouteXDosesAuStock(int x) {
        MachineACafe machineACafe = new BuilderMachineACafe().stockCafe(new NombrePositif(0)).build();
        NombrePositif stockInitialCafe = machineACafe.getStockCafe();
        NombrePositif stockCafeAttendu = stockInitialCafe.ajout(x);
        machineACafe.ajouterCafe(x);
        assertEquals(stockCafeAttendu, machineACafe.getStockCafe());
    }
    @ParameterizedTest
    @ValueSource(ints = {0,20})
    @DisplayName("ÉTANT DONNÉ une machine à café avec <x> gobelets QUAND on met une tasse ALORS le café coule ET le nombre de gobelets est inchangé")
    public void cafeCouleDansDonnerLeGobelet(int x) {
        MachineACafe machineACafe = new BuilderMachineACafe().stockGobelets(new NombrePositif(x)).build();
        NombrePositif nombreCafesServisInitial = machineACafe.getNombreCafesServis();
        NombrePositif nbDosesStockeesInitial = machineACafe.getStockCafe();
        NombrePositif stockGobeletsInitial = machineACafe.getNombreGobelets();
        machineACafe.insererMonnaie(40, true);
        assertEquals(machineACafe.getNombreCafesServis(), nombreCafesServisInitial.increment());
        assertEquals(nbDosesStockeesInitial.decrement(), machineACafe.getStockCafe());
        assertEquals(stockGobeletsInitial, machineACafe.getNombreGobelets());
    }

    @Test
    @DisplayName("QUAND on initialise une machine a café avec un stock de café initial supérieur à 30, ALORS une exception est lancée CAS 31")
    public void initialiserMachineAvecCafeHorsBornesLanceException() {
        assertThrows(IllegalArgumentException.class, () -> {
            MachineACafe machineACafe = new BuilderMachineACafe().stockCafe(new NombrePositif(31)).build();
        });
    }

    @Test
    @DisplayName("QUAND on initialise une machine a café avec un stock de sucre initial supérieur à 100, ALORS une exception est lancée CAS 101")
    public void initialiserMachineAvecSucreHorsBornesLanceException() {
        assertThrows(IllegalArgumentException.class, () -> {
            MachineACafe machineACafe = new BuilderMachineACafe().stockSucre(new NombrePositif(101)).build();
        });
    }

    @Test
    @DisplayName("QUAND on initialise une machine a café avec un stock de touillettes initial supérieur à 200, ALORS une exception est lancée CAS 201")
    public void initialiserMachineAvecTouillettesHorsBornesLanceException() {
        assertThrows(IllegalArgumentException.class, () -> {
            MachineACafe machineACafe = new BuilderMachineACafe().stockTouillettes(new NombrePositif(201)).build();
        });
    }
    @ParameterizedTest
	@NullSource
    @DisplayName("QUAND on initialise une machine a café avec un stock de gobelets initial égal à null, ALORS une exception est lancée")
    public void initialiserMachineAvecStockGobeletsNulLanceException(NombrePositif x) {
        assertThrows(NullPointerException.class, () -> {
            MachineACafe machineACafe = new BuilderMachineACafe().stockGobelets(x).build();
        });
    }

    @ParameterizedTest
	@NullSource
    @DisplayName("QUAND on initialise une machine a café avec un stock de café initial égal à null, ALORS une exception est lancée")
    public void initialiserMachineAvecStockCafeNulLanceException(NombrePositif x) {
        assertThrows(NullPointerException.class, () -> {
            MachineACafe machineACafe = new BuilderMachineACafe().stockCafe(x).build();
        });
    }

    @ParameterizedTest
	@NullSource
    @DisplayName("QUAND on initialise une machine a café avec un stock de sucre initial égal à null, ALORS une exception est lancée")
    public void initialiserMachineAvecStockSucreNulLanceException(NombrePositif x) {
        assertThrows(NullPointerException.class, () -> {
            MachineACafe machineACafe = new BuilderMachineACafe().stockSucre(x).build();
        });
    }

    @ParameterizedTest
	@NullSource
    @DisplayName("QUAND on initialise une machine a café avec un nombre de cafés servis initial égal à null, ALORS une exception est lancée")
    public void initialiserMachineAvecNombreCafesServisNulLanceException(NombrePositif x) {
        assertThrows(NullPointerException.class, () -> {
            MachineACafe machineACafe = new BuilderMachineACafe().nbCafesServis(x).build();
        });
    }

    @ParameterizedTest
	@NullSource
    @DisplayName("QUAND on initialise une machine a café avec une quantité d'argent encaissé initial égal à null, ALORS une exception est lancée")
    public void initialiserMachineAvecArgentEncaisseNulLanceException(NombrePositif x) {
        assertThrows(NullPointerException.class, () -> {
            MachineACafe machineACafe = new BuilderMachineACafe().argentEncaisse(x).build();
        });
    }

    @ParameterizedTest
	@NullSource
    @DisplayName("QUAND on initialise une machine a café avec un stock d'eau initial égal à null, ALORS une exception est lancée")
    public void initialiserMachineAvecStockEauNulLanceException(NombrePositif x) {
        assertThrows(NullPointerException.class, () -> {
            MachineACafe machineACafe = new BuilderMachineACafe().stockEau(x).build();
        });
    }

    @ParameterizedTest
	@NullSource
    @DisplayName("QUAND on initialise une machine a café avec un stock de touillettes initial égal à null, ALORS une exception est lancée")
    public void initialiserMachineAvecStockTouillettesNulLanceException(NombrePositif x) {
        assertThrows(NullPointerException.class, () -> {
            MachineACafe machineACafe = new BuilderMachineACafe().stockTouillettes(x).build();
        });
    }
    @Test
    @DisplayName("ETANT DONNÉ que le bouton \"sucre\" a été pressé 6 fois QUAND on met 40cts ALORS on obtient un café ET 5 doses de sucre seulement ont été consommées")
    public void QUANDOnAugmenteLaDose6FoisLeCafeCouleEt5DosesDeSucreSontConsommees() {
        MachineACafe machineACafe = new BuilderMachineACafe().build();
        NombrePositif nombreCafesServisInitial = machineACafe.getNombreCafesServis();
        NombrePositif nombreCafesServisAttendu = nombreCafesServisInitial.increment();
        NombrePositif stockCafeInitial = machineACafe.getStockCafe();
        NombrePositif stockCafeAttendu = stockCafeInitial.decrement();
        NombrePositif stockSucreInitial = machineACafe.getStockSucre();
        NombrePositif stockSucreAttendu = stockSucreInitial.retrait(5);
        for (int i = 0; i < 6; i++) {
            machineACafe.augmenterDoseSucre(); // On appuie sur le bouton + 6 fois
        }
        machineACafe.insererMonnaie(40,false);
        assertEquals(nombreCafesServisAttendu, machineACafe.getNombreCafesServis()); //Le nombre de cafés servis a augmenté de 1
        assertEquals(stockCafeAttendu, machineACafe.getStockCafe()); //Le nombre de doses de café a diminué de 1
        assertEquals(stockSucreAttendu, machineACafe.getStockSucre()); // Le nombre de doses de café a diminué de 5
    }
    @ParameterizedTest
    @ValueSource(ints = {1,2,3,4,5})
    @DisplayName("ETANT DONNÉ que le bouton \"sucre\" a été pressé <x> fois ET que le bouton de retrait de dose a été pressé <x> fois, QUAND on met 40cts ALORS on obtient un café ET aucune dose de sucre n'a été consommée")
    public void boutonAnnulationFonctionneEtRetireDesDoses(int x) {
        MachineACafe machineACafe = new BuilderMachineACafe().build();
        NombrePositif nombreCafesServisInitial = machineACafe.getNombreCafesServis();
        NombrePositif nombreCafesServisAttendu = nombreCafesServisInitial.increment();
        NombrePositif stockCafeInitial = machineACafe.getStockCafe();
        NombrePositif stockCafeAttendu = stockCafeInitial.decrement();
        NombrePositif stockSucreInitial = machineACafe.getStockSucre();
        for (int i = 0; i < x; i++) {
            machineACafe.augmenterDoseSucre();
        }
        for (int i = 0; i < x; i++) {
            machineACafe.diminuerDoseSucreCafeCourant();
        }
        machineACafe.insererMonnaie(40, false);
        assertEquals(nombreCafesServisAttendu, machineACafe.getNombreCafesServis()); //Le nombre de cafés servis a augmenté de 1
        assertEquals(stockCafeAttendu, machineACafe.getStockCafe()); //Le nombre de doses de café a diminué de 1
        assertEquals(stockSucreInitial, machineACafe.getStockSucre()); //Le stock de sucre n'a pas changé
    }
    //Dernier test : goûter le café pour voir s'il est bon ! :-)
}