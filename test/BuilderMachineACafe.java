import java.util.Optional;

public class BuilderMachineACafe {

    private NombrePositif stockSucre;
    private NombrePositif stockGobelets;
    private NombrePositif nbCafesServis;
    private NombrePositif argentEncaisse;
    private NombrePositif stockEau;
    private NombrePositif stockTouilettes;
    private NombrePositif dosesDeSucreCafeCourant;
    private NombrePositif stockCafe;

    public BuilderMachineACafe() {
        this.stockCafe = new NombrePositif(10);
        this.stockSucre = new NombrePositif(10);
        this.stockEau = new NombrePositif(10);
        this.stockGobelets = new NombrePositif(10);
        this.stockTouilettes = new NombrePositif(10);
        this.nbCafesServis = new NombrePositif(0);
        this.argentEncaisse = new NombrePositif(0);
        this.dosesDeSucreCafeCourant = new NombrePositif(0);
    }

    public BuilderMachineACafe stockSucre(NombrePositif i) {
        this.stockSucre = i;
        return this;
    }

    public MachineACafe build() {
        return new MachineACafe(this.stockGobelets, this.stockCafe,this.nbCafesServis,this.argentEncaisse,this.stockEau,this.stockSucre, this.stockTouilettes);
    }

    public BuilderMachineACafe stockEau(NombrePositif i) {
        this.stockEau = i;
        return this;
    }

    public BuilderMachineACafe stockTouillettes(NombrePositif i) {
        this.stockTouilettes = i;
        return this;
    }

    public BuilderMachineACafe argentEncaisse(NombrePositif argentEncaisse) {
        this.argentEncaisse = argentEncaisse;
        return this;
    }

    public BuilderMachineACafe nbCafesServis(NombrePositif i) {
        this.nbCafesServis = i;
        return this;
    }

    public BuilderMachineACafe stockGobelets(NombrePositif stockGobelets) {
        this.stockGobelets = stockGobelets;
        return this;
    }

    public BuilderMachineACafe stockCafe(NombrePositif stockCafe) {
        this.stockCafe = stockCafe;
        return this;
    }
}
